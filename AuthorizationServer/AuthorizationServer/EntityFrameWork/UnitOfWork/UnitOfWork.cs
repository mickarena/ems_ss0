﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.Repositories.Service;

namespace AuthorizationServer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        //ICustomerRepository _customers;
        //IItemallocationRepository _itemallocations;
        //ICustomerBlackListRepository _customerBlackLists;
        //ICustomerDiscountRepository _customerDiscounts;
        //IItemCustomerLimitRepository _itemCustomerLimits;
        //IItemPoscodeLimitRepository _itemPoscodeLimits;
        //IItemRepository _items;
        //IParentCustomerRepository _parentCustomers;
        //ISysvarRepository _sysvars;
        IProvinceRepository _provinces;
        //IDistrictRepository _districts;
        //IServiceRepository _services;


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        //public ICustomerRepository Customers
        //{
        //    get
        //    {
        //        if (_customers == null)
        //            _customers = new CustomerRepository(_context);

        //        return _customers;
        //    }
        //}

        //public IItemallocationRepository Itemallocations
        //{
        //    get
        //    {
        //        if (_itemallocations == null)
        //            _itemallocations = new ItemallocationRepository(_context);

        //        return _itemallocations;
        //    }
        //}

        //public ICustomerBlackListRepository CustomerBlackLists
        //{
        //    get
        //    {
        //        if (_customerBlackLists == null)
        //            _customerBlackLists = new CustomerBlackListRepository(_context);
        //        return _customerBlackLists;
        //    }
        //}

        //public ICustomerDiscountRepository CustomerDiscounts
        //{
        //    get
        //    {
        //        if (_customerDiscounts == null)
        //            _customerDiscounts = new CustomerDiscountRepository(_context);
        //        return _customerDiscounts;
        //    }
        //}

        //public IItemCustomerLimitRepository ItemCustomerLimits
        //{
        //    get
        //    {
        //        if (_itemCustomerLimits == null)
        //            _itemCustomerLimits = new ItemCustomerLimitRepository(_context);
        //        return _itemCustomerLimits;
        //    }
        //}

        //public IItemPoscodeLimitRepository ItemPoscodeLimits
        //{
        //    get
        //    {
        //        if (_itemPoscodeLimits == null)
        //            _itemPoscodeLimits = new ItemPoscodeLimitRepository(_context);
        //        return _itemPoscodeLimits;
        //    }
        //}

        //public IItemRepository Items
        //{
        //    get
        //    {
        //        if (_items == null)
        //            _items = new ItemRepository(_context);
        //        return _items;
        //    }
        //}

        //public IParentCustomerRepository ParentCustomers
        //{
        //    get
        //    {
        //        if (_parentCustomers == null)
        //            _parentCustomers = new ParentCustomerRepository(_context);
        //        return _parentCustomers;
        //    }
        //}

        //public ISysvarRepository Sysvars
        //{
        //    get
        //    {
        //        if (_sysvars == null)
        //            _sysvars = new SysvarRepository(_context);
        //        return _sysvars;
        //    }
        //}


        public IProvinceRepository Provinces
        {
            get
            {
                if (_provinces == null)
                    _provinces = new ProvinceRepository(_context);
                return _provinces;
            }
        }


        //public IDistrictRepository Districts
        //{
        //    get
        //    {
        //        if (_districts == null)
        //            _districts = new DistrictRepository(_context);
        //        return _districts;
        //    }
        //}

        //public IServiceRepository Services
        //{
        //    get
        //    {
        //        if (_services == null)
        //            _services = new ServiceRepository(_context);
        //        return _services;
        //    }
        //}



        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
