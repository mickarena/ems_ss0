﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================


using AuthorizationServer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationServer.UnitOfWork
{
    public interface IUnitOfWork
    {
        //ICustomerRepository Customers { get; }
        //IItemallocationRepository Itemallocations { get; }
        //ICustomerBlackListRepository CustomerBlackLists { get; }
        //ICustomerDiscountRepository CustomerDiscounts { get; }
        //IItemCustomerLimitRepository ItemCustomerLimits { get; }
        //IItemPoscodeLimitRepository ItemPoscodeLimits { get; }
        //IItemRepository Items { get; }
        //IParentCustomerRepository ParentCustomers { get; }
        //ISysvarRepository Sysvars { get; }

        IProvinceRepository Provinces { get; }
        //IServiceRepository Services { get; }
        //IDistrictRepository Districts { get; }

        int SaveChanges();
    }
}
