﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AuthorizationServer.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} phải dài ít nhất {2} ký tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Mật khẩu và mật khẩu xác nhận không khớp.")]
        public string ConfirmPassword { get; set; }

        [DisplayName("Họ tên")]
        [Required(ErrorMessage = "Bạn phải điền thông tin họ tên.")]
        public string FullName { get; set; }
        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "Bạn phải điền số điện thoại.")]
        public string PhoneNumber { get; set; }


    }
}
