﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationServer.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Không được để trống Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Không được để trống Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
