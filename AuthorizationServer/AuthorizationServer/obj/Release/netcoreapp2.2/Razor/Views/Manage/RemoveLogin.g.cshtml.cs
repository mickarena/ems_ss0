#pragma checksum "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0fea34970d4a6ebb633f0d32aebef3df521ee9cf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Manage_RemoveLogin), @"mvc.1.0.view", @"/Views/Manage/RemoveLogin.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Manage/RemoveLogin.cshtml", typeof(AspNetCore.Views_Manage_RemoveLogin))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer;

#line default
#line hidden
#line 2 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.Models;

#line default
#line hidden
#line 3 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Account;

#line default
#line hidden
#line 4 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Authorization;

#line default
#line hidden
#line 5 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Manage;

#line default
#line hidden
#line 6 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Shared;

#line default
#line hidden
#line 7 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0fea34970d4a6ebb633f0d32aebef3df521ee9cf", @"/Views/Manage/RemoveLogin.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d0c565365258a8e4c849e9a3267a1d7f70e92b1f", @"/Views/_ViewImports.cshtml")]
    public class Views_Manage_RemoveLogin : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ICollection<Microsoft.AspNetCore.Identity.UserLoginInfo>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Manage", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "RemoveLogin", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-horizontal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
  
    ViewData["Title"] = "Remove Login";

#line default
#line hidden
            BeginContext(109, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 6 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
 if (Model.Count > 0)
{

#line default
#line hidden
            BeginContext(134, 73, true);
            WriteLiteral("    <h4>Registered Logins</h4>\n    <table class=\"table\">\n        <tbody>\n");
            EndContext();
#line 11 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
             foreach (var account in Model)
            {

#line default
#line hidden
            BeginContext(265, 45, true);
            WriteLiteral("                <tr>\n                    <td>");
            EndContext();
            BeginContext(311, 21, false);
#line 14 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
                   Write(account.LoginProvider);

#line default
#line hidden
            EndContext();
            BeginContext(332, 31, true);
            WriteLiteral("</td>\n                    <td>\n");
            EndContext();
#line 16 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
                         if ((bool)ViewData["ShowRemoveButton"])
                        {

#line default
#line hidden
            BeginContext(454, 28, true);
            WriteLiteral("                            ");
            EndContext();
            BeginContext(482, 613, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0fea34970d4a6ebb633f0d32aebef3df521ee9cf7977", async() => {
                BeginContext(632, 75, true);
                WriteLiteral("\n                                <div>\n                                    ");
                EndContext();
                BeginContext(707, 56, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0fea34970d4a6ebb633f0d32aebef3df521ee9cf8432", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 20 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => account.LoginProvider);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(763, 37, true);
                WriteLiteral("\n                                    ");
                EndContext();
                BeginContext(800, 54, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0fea34970d4a6ebb633f0d32aebef3df521ee9cf10305", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 21 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => account.ProviderKey);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(854, 96, true);
                WriteLiteral("\n                                    <input type=\"submit\" class=\"btn btn-default\" value=\"Remove\"");
                EndContext();
                BeginWriteAttribute("title", " title=\"", 950, "\"", 1016, 7);
                WriteAttributeValue("", 958, "Remove", 958, 6, true);
                WriteAttributeValue(" ", 964, "this", 965, 5, true);
#line 22 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
WriteAttributeValue(" ", 969, account.LoginProvider, 970, 22, false);

#line default
#line hidden
                WriteAttributeValue(" ", 992, "login", 993, 6, true);
                WriteAttributeValue(" ", 998, "from", 999, 5, true);
                WriteAttributeValue(" ", 1003, "your", 1004, 5, true);
                WriteAttributeValue(" ", 1008, "account", 1009, 8, true);
                EndWriteAttribute();
                BeginContext(1017, 71, true);
                WriteLiteral(" />\n                                </div>\n                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-returnurl", "Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 18 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
                                                                                            WriteLiteral(ViewData["ReturnUrl"]);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["returnurl"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-returnurl", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["returnurl"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1095, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 25 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
                        }
                        else
                        {

#line default
#line hidden
            BeginContext(1177, 28, true);
            WriteLiteral("                            ");
            EndContext();
            BeginContext(1207, 8, true);
            WriteLiteral(" &nbsp;\n");
            EndContext();
#line 29 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
                        }

#line default
#line hidden
            BeginContext(1241, 48, true);
            WriteLiteral("                    </td>\n                </tr>\n");
            EndContext();
#line 32 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
            }

#line default
#line hidden
            BeginContext(1303, 30, true);
            WriteLiteral("        </tbody>\n    </table>\n");
            EndContext();
#line 35 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Manage\RemoveLogin.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ICollection<Microsoft.AspNetCore.Identity.UserLoginInfo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
