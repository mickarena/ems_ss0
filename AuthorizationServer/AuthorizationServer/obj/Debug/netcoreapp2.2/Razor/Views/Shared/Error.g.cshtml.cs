#pragma checksum "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "658722f61396c0d362a25454aca584f61969a584"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Error), @"mvc.1.0.view", @"/Views/Shared/Error.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Error.cshtml", typeof(AspNetCore.Views_Shared_Error))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer;

#line default
#line hidden
#line 2 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.Models;

#line default
#line hidden
#line 3 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Account;

#line default
#line hidden
#line 4 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Authorization;

#line default
#line hidden
#line 5 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Manage;

#line default
#line hidden
#line 6 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using AuthorizationServer.ViewModels.Shared;

#line default
#line hidden
#line 7 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"658722f61396c0d362a25454aca584f61969a584", @"/Views/Shared/Error.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d0c565365258a8e4c849e9a3267a1d7f70e92b1f", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Error : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ErrorViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(22, 112, true);
            WriteLiteral("    \n<div class=\"jumbotron\">\n    <h2>Ooooops, something went really bad! :(</h2>\n    <p class=\"lead text-left\">\n");
            EndContext();
#line 6 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
         if (!string.IsNullOrEmpty(Model.Error)) {

#line default
#line hidden
            BeginContext(185, 20, true);
            WriteLiteral("            <strong>");
            EndContext();
            BeginContext(206, 11, false);
#line 7 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
               Write(Model.Error);

#line default
#line hidden
            EndContext();
            BeginContext(217, 10, true);
            WriteLiteral("</strong>\n");
            EndContext();
#line 8 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
        }

#line default
#line hidden
            BeginContext(237, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 10 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
         if (!string.IsNullOrEmpty(Model.ErrorDescription)) {

#line default
#line hidden
            BeginContext(300, 19, true);
            WriteLiteral("            <small>");
            EndContext();
            BeginContext(320, 22, false);
#line 11 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
              Write(Model.ErrorDescription);

#line default
#line hidden
            EndContext();
            BeginContext(342, 9, true);
            WriteLiteral("</small>\n");
            EndContext();
#line 12 "D:\SOURCECODE\ems_ss0\AuthorizationServer\AuthorizationServer\Views\Shared\Error.cshtml"
        }

#line default
#line hidden
            BeginContext(361, 15, true);
            WriteLiteral("    </p>\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ErrorViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
