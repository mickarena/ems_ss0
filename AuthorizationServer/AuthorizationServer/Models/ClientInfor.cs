﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.Models
{
    public class ClientInfor
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string DisplayName { get; set; }
        public List<string>  RedirectUris { get; set; }
        public List<string> PostLogoutRedirectUris { get; set; }
        

    }
}
