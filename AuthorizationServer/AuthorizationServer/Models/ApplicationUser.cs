﻿using Microsoft.AspNetCore.Identity;
using System;

namespace AuthorizationServer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser {
        public string PosCode { get; set; }
        public string DistrictCode { get; set; }
        public string ProvincialCode { get; set; }
        public DateTime? Birthday { get; set; }
        public string FullName { get; set; }
    }
}
