﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.Models
{
    [Table("Province")]
    public class Province
    {
        [Key]
        [Required]
        [Column(TypeName = "varchar(3)")]
        public string ProvinceCode { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string ProvinceName { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string Description { get; set; }
        [Column(TypeName = "varchar(2)")]
        public string RegionCode { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string ProvinceListCode { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Longtitude { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Lattitude { get; set; }
    }
}
