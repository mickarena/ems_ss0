using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace ClientApp
{
    public class Startup
    {


        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {

            _configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })

            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/signin");
            })

            .AddOpenIdConnect(options =>
            {
                // Note: these settings must match the application details
                // inserted in the database at the server level.
                options.ClientId = _configuration.GetValue<string>("ClientId");// "resource-server-1";
                options.ClientSecret = _configuration.GetValue<string>("ClientSecret");// "846B62D0-DEF9-4215-A99D-86E6B8DAB342";


                options.Events.OnRedirectToIdentityProvider = async n =>
                {
                    n.ProtocolMessage.RedirectUri = (new Uri(_configuration.GetValue<string>("ClientUrl") + "signin-oidc")).ToString();
                    n.ProtocolMessage.PostLogoutRedirectUri = (new Uri(_configuration.GetValue<string>("ClientUrl") + "signout-callback-oidc")).ToString();
                    await Task.FromResult(0);
                };


                options.RequireHttpsMetadata = false;
                options.GetClaimsFromUserInfoEndpoint = true;
                options.SaveTokens = true;

                // Use the authorization code flow.
                options.ResponseType = OpenIdConnectResponseType.Code;
                options.AuthenticationMethod = OpenIdConnectRedirectBehavior.RedirectGet;

                // Note: setting the Authority allows the OIDC client middleware to automatically
                // retrieve the identity provider's configuration and spare you from setting
                // the different endpoints URIs or the token validation parameters explicitly.
                ///options.Authority = "http://210.245.87.249:54540/";
                //options.Authority = "http://localhost:54540/";
                //options.Authority = "https://localhost:443/";


                options.Authority = _configuration.GetValue<string>("ServerAuthenUrl").ToString();

                options.Scope.Add("email");
                options.Scope.Add("roles");
                options.Scope.Add("profile");

                options.SecurityTokenValidator = new JwtSecurityTokenHandler
                {
                    // Disable the built-in JWT claims mapping feature.
                    InboundClaimTypeMap = new Dictionary<string, string>()
                };

                options.TokenValidationParameters.NameClaimType = "name";
                options.TokenValidationParameters.RoleClaimType = "role";
            });

            services.AddMvc();

            services.AddSingleton<HttpClient>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}